from jwcrypto import jwt, jwk
from datetime import datetime, timedelta
import json
import pickle
import os
import ast


class JWTDomain:

    def __init__(self):
        self.today = datetime.now()
        self.today_plus_one_minute = self.today + timedelta(minutes=1)
        self.today_plus_one_day = self.today + timedelta(days=1)
        self.algs_signature = ['RS512']
        self.key = None

    def generate_key(self):
        key_generated = jwk.JWK.generate(kty='RSA', size=1024)
        self.key = key_generated

    def generate_jwt(self, username):
        jwt_returned = None
        try:
            self.generate_key()
            key = self.key
            print(key.export_to_pem(private_key=True,password=None))
            print(key.export_to_pem())
            if not key:
                raise
            header = {'alg': 'RS512'}
            claims = {
                'username': username,
                '1078571': '7984588',
                '8680694': 'Basic',
                'sub': username
            }
            default_claims = {
                'exp': self.today_plus_one_day.timestamp(),
                'aud': 'trial',
                'iss': 'ForensicT',
                'nbf': self.today_plus_one_minute.timestamp(),
                'iat': self.today.timestamp()
            }
            token = jwt.JWT(header=header, claims=claims, default_claims=default_claims)
            token.make_signed_token(key)
            jwt_returned = token.serialize()
        except Exception as error:
            print(error)
            jwt_returned = None
        return jwt_returned

    def verify_token_integrity(self, token):
        message = None
        try:
            key = self.load_file_with_key()
            key_dict = json.loads(key.export_private())
            key = jwk.JWK(**key_dict)
            header, payload = self.get_token_information(token, key)
            check_claims = {
                'exp': datetime.now().timestamp(),
                'aud': 'trial',
                'iss': 'ForensicT',
                'nbf': self.today_plus_one_minute.timestamp(),
                'iat': None
            }
            is_valid = self.token_verify_with_iat(payload['iat'], payload['exp'], check_claims['exp'])
            if not is_valid:
                raise Exception('invalid token')
            iat_calculated = payload['exp'] - 86400
            check_claims['iat'] = iat_calculated
            payload = jwt.JWT(key=key, jwt=token, check_claims=check_claims, algs=self.algs_signature)
            message = ast.literal_eval(payload.claims)
        except Exception:
            message = None
        return message

    def get_token_information(self, token, key):
        header, payload = None, None
        try:
            information = jwt.JWT(key=key, jwt=token)
            header = ast.literal_eval(information.header)
            payload = ast.literal_eval(information.claims)
        except Exception:
            header, payload = None, None
        return header, payload

