from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
from Crypto import Cipher

message_original = b'Public and Private keys encryption'

def asymmetric_encrypt():
    private_key = RSA.generate(2048)
    public_key = private_key.publickey()
    file_ = open('pri_key.pem','wb')
    file_.write(private_key.export_key('PEM'))
    file_ = open('pub_key.pem','wb')
    file_.write(public_key.export_key('PEM'))
    #print(private_key.export_key())
    #print(public_key.export_key())
    file_.close()
    encrypt_padding = PKCS1_OAEP.new(key=public_key)
    message_encrypted = encrypt_padding.encrypt(message_original)
    print(message_original)
    print(message_encrypted.hex())
    return private_key, public_key

def asymmetric_decrypt(message_encrypted):

    pr_key = RSA.import_key(open('pri_key.pem', 'r').read()) 
    decrypt_padding = PKCS1_OAEP.new(key=pr_key)
    message_decrypted = decrypt_padding.decrypt(message_encrypted)
    print(message_decrypted)
    return message_decrypted
